# Solr Search Metadata

A fork of the [SearchByMetadata](https://github.com/omeka/plugin-SearchByMetadata) "official" Omeka plugin, to generate links based on the SolrSearch Engine instead of Omeka's default search engine.

## Requirements

This forked plugin will work anyhow, but using the alternate links requires the [SolrSearch Plugin](https://github.com/scholarslab/SolrSearch) to be installed and enabled.
**The switch between default and Solr Search engine can be done anytime** in the plugin configuration page (`Plugins` -> `Solr Search By Metadata` -< `config`)

## Install

Like any plugin :
1. clone or download and unzip this repo in your omeka's `plugins/` folder.
2. Rename the folder exactly `SolrSearchByMetadata`
3. Go to the `Plugins` page, and install `Solr Search By Metadata`

## Acknowledgement and license

This fork is entirely based on the work of
- the developers of Omeka and the [SearchByMetadata plugin](https://github.com/omeka/plugin-SearchByMetadata) at the [Corporation for Digital Scholarship](http://digitalscholar.org/) (Omeka is licensed under the GNU General Public License v3.0, and so is this fork).
- the developers of the [SolrSearch plugin](https://github.com/scholarslab/SolrSearch) at the [University of Charlottesville (Virginia) Scholars' Lab ]( http://www.scholarslab.org) (licensed under Apache License 2.0).
